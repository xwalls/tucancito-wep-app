module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
    ? '/tucancito-wep-app/'
    : '/',
  }